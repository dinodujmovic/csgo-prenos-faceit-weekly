import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService, PLAYERS} from './data.service';
import * as _ from 'lodash';
import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public loadFinish = false;
  public players = [];
  public selectedFilter = 'bestPerformance';

  constructor(private dataService: DataService) {
    this.players = PLAYERS;
  }

  ngOnInit(): void {
    const promises = [];
    // Iterate through players
    _.each(this.players, (player: any) => {
      const promise = new Promise(resolve => {

        this.dataService.getPlayerDetails(player.id)
          .toPromise()
          .then((playerDetails: any) => {
            player.image = playerDetails.avatar;
            player.faceit_url = playerDetails.faceit_url.replace('{lang}', 'en');
            player.nickname = playerDetails.nickname;

            this.dataService.getPlayerHistory(player.id)
              .toPromise()
              .then((response: any) => {
                const matches = _.filter(response.items, (match) => {
                  return match.teams_size === 5 && match.competition_type === 'matchmaking';
                });

                // Init additional player data
                player.stats = {
                  matchesPlayed: matches.length,
                  assists: 0,
                  headshots: 0,
                  kills: 0,
                  tripleKills: 0,
                  quadroKills: 0,
                  pentaKills: 0,
                  MVPs: 0
                };

                const playersMatchesRequests = [];
                // Iterate through matches
                _.each(matches, (match) => {
                  playersMatchesRequests.push(this.dataService.getMatchInfo(match.match_id));
                });

                if (!playersMatchesRequests.length) {
                  resolve(true);
                }
                // Get all matches for the player (wait until all resolved)
                forkJoin(playersMatchesRequests).toPromise().then((allMatches: any) => {
                  _.each(allMatches, (match) => {
                    let myPlayer;
                    const matchInfo = match.rounds[0];
                    // Find current player stats inside of two teams
                    _.each(matchInfo.teams, (team) => {
                      const playerFound = _.find(team.players, {player_id: player.id});
                      if (playerFound) {
                        myPlayer = playerFound;
                        return false;
                      }
                    });

                    // Calculate statistics here
                    if (myPlayer && myPlayer.player_stats) {
                      player.stats.assists += +myPlayer.player_stats.Assists;
                      player.stats.headshots += +myPlayer.player_stats.Headshot;
                      player.stats.kills += +myPlayer.player_stats.Kills;
                      player.stats.tripleKills += +myPlayer.player_stats['Triple Kills'];
                      player.stats.quadroKills += +myPlayer.player_stats['Quadro Kills'];
                      player.stats.pentaKills += +myPlayer.player_stats['Penta Kills'];
                      player.stats.MVPs += +myPlayer.player_stats.MVPs;
                    }

                    resolve(true);
                  });


                  player.stats.avgKills = (player.stats.kills / player.stats.matchesPlayed);
                  player.stats.avgAssists = (player.stats.assists / player.stats.matchesPlayed);
                  player.stats.avgHeadshots = (player.stats.headshots / player.stats.matchesPlayed);
                  player.stats.avgTripleKills = (player.stats.tripleKills / player.stats.matchesPlayed);
                  player.stats.avgQuadroKills = (player.stats.quadroKills / player.stats.matchesPlayed);
                  player.stats.avgPentaKills = (player.stats.pentaKills / player.stats.matchesPlayed);
                  player.stats.avgMVPs = (player.stats.MVPs / player.stats.matchesPlayed);
                  player.stats.bestPerformance = (player.stats.avgKills * 2) + (player.stats.avgAssists * 1.5) + (player.stats.matchesPlayed * 0.5)
                  console.log(player);
                });
              });
          });
      });

      promises.push(promise);
    });

    Promise.all(promises).then(() => {
      console.log('Resolved');
      this.players = _.filter(this.players, (player) => {
        return player.stats.matchesPlayed > 0;
      });
      this.filterChanged(this.selectedFilter);
      this.loadFinish = true;
    });
  }

  public filterChanged(filter: string) {
    this.players = _.orderBy(this.players, (player) => player.stats[filter], ['desc']);
  }

  public openProfile(url) {
    window.open(
      url,
      '_blank' // <- This is what makes it open in a new window.
    );
  }
}
