import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as moment from 'moment';

export const PLAYERS = [
  {
    id: 'fe248073-971d-4bc0-aa43-0f5edc7070ba',
    name: 'Zoka',
  },
  {
    id: 'a017f031-50cd-4223-aa94-49a804fbb270',
    name: 'Ivo',
  },
  {
    id: '064135bd-9467-4b94-ac09-41722e36f09d',
    name: 'Drazen',
  },
  {
    id: '708a1ec6-3daa-4113-a494-1b7146f1e818',
    name: 'Dino',
  },
  {
    id: '2fec49e4-b8d4-424f-8c5b-f712d2909534',
    name: 'Vice',
  },
  {
    id: '3ad3b6ef-2884-450b-a5d2-aabeadcab1e0',
    name: 'Milan',
  },
  {
    id: '6a273133-da7b-430b-afb8-438e509cfabd',
    name: 'Nedim',
  },
  {
    id: 'dc00a9f3-bd17-4f95-98ee-9bf7634eb766',
    name: 'Mario',
  },
  {
    id: 'cba5947b-6099-44e4-ba12-2dfcf85bd981',
    name: 'Dono',
  },
  {
    id: 'fbb6b4db-ecb2-4948-af3d-7094431cb204',
    name: 'Tonac'
  }
];

@Injectable()
export class DataService {
  private baseURL: string;
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.baseURL = 'https://open.faceit.com/data/v4/'
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer 9298192b-3480-4772-9dcf-74ac8c275b0d')
  }

  getPlayerDetails(playerId: string) {
    return this.http
      .get(`${this.baseURL}players/${playerId}`,
        {headers: this.headers});
  }

  getPlayerHistory(playerId: string) {
    return this.http
      .get(`${this.baseURL}players/${playerId}/history?limit=100&from=${moment().subtract(7, 'd').unix()}&to=${moment().unix()}`,
        {headers: this.headers});
  }

  getMatchInfo(matchId: string) {
    return this.http.get(`${this.baseURL}matches/${matchId}/stats`, {headers: this.headers});
  }
}
